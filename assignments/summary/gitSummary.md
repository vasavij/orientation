
# Git


* **Git** is a distributed version-control system for tracking changes in source code during software development. 
* It is designed for coordinating work among programmers, but it can be used to track changes in any set of files.
* Its goals include speed, data integrity, and support for distributed, non-linear workflows.


**Workspace** : All the changes are done in this tree of repository.

**Staging** : All the staged files(ready to be committed) go into this tree of our repository.

**Local Repository** : All the committed files go to this tree of repository.

**Remote Repository** : This is the copy of our Local Repository stored on server on Internet.

**Basic Git commands**:

- git init - _create a new loca repository_.
- git clone  : _clone the repository on local system_.
- git add . : _adds changes in the working directory to the staging area_.
- git commit : _saves the files from staging area into the local Git repository_.
- git push origin master : _saves the files in local repo onto the remote repo_.
- git pull - _fetch and merge changes on the remote server to your working directory_.

## Git Workflow :

A Git Workflow is a recipe or recommendation for how to use Git to accomplish work in a consistent and productive manner.

Git workflows encourage users to leverage Git effectively and consistently. Git offers a lot of flexibility in how users manage changes.

![git](https://gitlab.iotiot.in/newbies/orientation/wikis/extras/Git.png)

### Steps:

* **Clone** the central repository - 

>git clone repository-name

* Create a new **branch** -

>git checkout master

>git checkout -b your-branch-name

* **Modify** files in your working tree.

* Selectively **stage** just those changes you want to be part of your next commit.

>git add .

* Do a **commit**, to store the files as they are in the staging area permanently to your Local Git Repository.

>git commit -sv

* Do a **push**, to store the new committed changes to the central repository.

>git push origin branch-name
